package com.fortunes.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.fortunes.util.StringUtils;

/**
 * @author gongchaobin
 * 
 *         日期工具类
 * @version 2012-12-19
 */
public class DateUtil {
	@SuppressWarnings("unused")
	private static final String TAG = DateUtil.class.getSimpleName();

	/**
	 * @return xxx年xxx月
	 */
	public static String getYearMonth() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		return year + "年" + month + "月";
	}

	/**
	 * @return xxx年xxx月
	 */
	public static String getYearMonth(Calendar calendar) {
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONDAY) + 1;
		return year + "年" + month + "月";
	}

	/**
	 * @return 当前日期(YYYY-MM-DD)
	 */
	public static String getYearMonthDay() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
		return year + "-" + month + "-" + day;
	}

	/**
	 * 把calendar转化为当前年月日
	 * 
	 * @param calendar
	 *            Calendar
	 * @return 返回成转换好的 年月日格式
	 */
	public static String getDay(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.format(date);
	}

	/**
	 * @param date
	 * @return 返回当前时间点数(HH:mm)
	 */
	public static String getTime(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		return simpleDateFormat.format(date);
	}

	/**
	 * 获取精确到分钟的时间
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateByMinute(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return simpleDateFormat.format(date);
	}

	/** 通过当天字符串日期得到当月最后一天 **/
	public static String getNextDay(String date) {
		Calendar cal = Calendar.getInstance();// 获取当前日期
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date));
			cal.add(Calendar.DATE, 1);// 天增加1天
			return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(cal.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 通过当天字符串日期得到当月最后一天 **/
	public static String getLastDateByMonth(String date) {
		Calendar cal = Calendar.getInstance();// 获取当前日期
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			cal.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
			cal.add(Calendar.MONTH, 1);// 月增加1天
			cal.add(Calendar.DAY_OF_MONTH, -1);// 日期倒数一日,既得到本月最后一天
			return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 通过当天字符串日期得到当月第一天 */
	public static String getFirstDateByMonth(String date) {
		Calendar cal = Calendar.getInstance();// 获取当前日期
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			cal.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
			return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 通过字符串日期获得下一个月日期字符串 */
	public static String getNextMonth(String date) {
		Calendar cal = Calendar.getInstance();// 获取当前日期
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			cal.add(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
			return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 通过字符串日期获得上一个月日期字符串 */
	public static String getPreMonth(String date) {
		Calendar cal = Calendar.getInstance();// 获取当前日期
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
			return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将xx/x/xx hh:mm:ss转化为xx-xx-xx hh:mm:ss
	 * 
	 * @param time
	 * @return
	 */
	public static String toNewDate(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = null;
		try {
			date = format.parse(time);
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return format2.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * xx-xx-xx 转换为xxxx年mm月
	 * 
	 * @param time
	 *            格式为"yyyy-MM-dd"
	 * @return
	 */
	public static String toChineseNewMonth(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(time);
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy年MM月");
			return format2.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * xx-xx-xx 转换为MM月dd日
	 * 
	 * @param time
	 * @return
	 */
	public static String toChineseNewMonthDay(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(time);
			SimpleDateFormat format2 = new SimpleDateFormat("MM月dd日");
			return format2.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/***
	 * 返回指定形式的字符串
	 * 
	 * @param time
	 *            格式：yyyy-MM-dd HH:mm:ss
	 * @param Pattern
	 *            要转换的格式
	 * @return
	 */
	public static String getStrByPattern(String time, String Pattern) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = format.parse(time);
			SimpleDateFormat format2 = new SimpleDateFormat(Pattern);
			return format2.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description 获取当前月份的第一天
	 * @param format
	 *            日期格式，传空默认为中文"yyyy年MM月dd日"
	 * @return
	 */
	public static String startDate(String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		SimpleDateFormat shortSdf = null;
		if (StringUtils.isEmpty(format)) {
			shortSdf = new SimpleDateFormat("yyyy年MM月dd日");
		} else {
			shortSdf = new SimpleDateFormat(format);
		}

		return shortSdf.format(calendar.getTime());
	}

	/**
	 * @Description 获取当前月份的最后一天
	 * @param format
	 *            日期格式，传空默认为中文"yyyy年MM月dd日"
	 * @return
	 */
	public static String endDate(String format) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat shortSdf = null;
		if (StringUtils.isEmpty(format)) {
			shortSdf = new SimpleDateFormat("yyyy年MM月dd日");
		} else {
			shortSdf = new SimpleDateFormat(format);

		}
		calendar.set(Calendar.DATE, 1);
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		return shortSdf.format(calendar.getTime());
	}

	/**
	 * 判断日期是否在当前月份
	 * 
	 * @param time
	 *            格式为"yyyy-MM-dd"
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isCurrentMonth(String time) {
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = shortSdf.parse(time);
			Calendar calendar = Calendar.getInstance();
			int month = calendar.get(Calendar.MONTH);

			if (date.getMonth() == month) {
				return true;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 获取当前中文日期(MM月DD日)
	 * 
	 * @return
	 */
	public static String getTodayMonthDay() {
		long l = System.currentTimeMillis();
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日 ");
		return sdf.format(date);
	}

	/** 时间毫秒数转时间yyyy-MM-dd HH:mm **/
	public static String getTimeByLong(String time) {
		long l = Long.parseLong(time);
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	public static String getTimeByLongEx(String time) {
		String newDateTime = addZeroForNum(time + "");
		long l = Long.parseLong(newDateTime);
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		return sdf.format(date);
	}

	/** 时间毫秒数转时间yyyy-MM-dd HH:mm **/
	public static String getTimeByLongNoSecoundEx(String time) {
		String newDateTime = addZeroForNum(time + "");
		long l = Long.parseLong(newDateTime);
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(date);
	}

	/** 时间毫秒数转时间yyyy-MM-dd HH:mm **/
	public static String getTimeByLong(String time, String format) {
		long l = Long.parseLong(time);
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/*** 时间秒数转换为yyyy-MM-dd HH:mm ***/
	public static String getTimeBySecond(String time) {
		long l = Long.parseLong(time) * 1000;
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 * @param time
	 * @return 时间毫秒数转时间yyyy-MM-dd HH:mm
	 */
	public static String getTimeByHHMM(String time) {
		long l = Long.parseLong(time);
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		return sdf.format(date);
	}

	/**
	 * 时间毫秒数转时间yyyy-MM-dd HH:mm
	 * 
	 * @param time
	 * @return
	 */
	public static String getTimeByHHMMx(String time) {
		long l = Long.parseLong(time) * 1000;
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(date);
	}

	/**
	 * 时间毫秒数转时间yyyy-MM-dd
	 * 
	 * @param time
	 * @return
	 */
	public static String getTimeByDay(String time) {
		long l = Long.parseLong(time) * 1000;
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(date);
	}

	/**
	 * 时间毫秒数转时间yyyy-MM-dd
	 * 
	 * @param time
	 * @return
	 */
	public static String getTimeByDayx(String time) {
		long l = Long.parseLong(time) * 1000;
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/** 根据时间值获取月日 **/
	public static String getMonthDayByTime(long time) {
		long l = time * 1000;
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
		return sdf.format(date);
	}

	/** 获取当年的年份 **/
	@SuppressWarnings("deprecation")
	public static int getYearByTime(long time) {
		long l = time * 1000;
		Date date = new Date(l);
		return date.getYear() + 1900;
	}

	/**
	 * 将字符串时间转换成毫秒数
	 * 
	 * @param time
	 * @return
	 */
	public static long getLongByTime(String time) {
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return c.getTimeInMillis();
	}

	public static long getLongByTimeEx(String time) {
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(new SimpleDateFormat("yyyy年MM月dd日 HH:mm").parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return c.getTimeInMillis();
	}

	/**
	 * 根据时间yyyy-MM-dd HH:mm:ss获取时间戳
	 */
	public static String getLongTime(String time) {
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.valueOf(c.getTimeInMillis());
	}

	/**
	 * 根据时间yyyy-MM-dd HH:mm:ss获取时间戳
	 */
	public static String getLongTime(String time, String format) {
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(new SimpleDateFormat(format).parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return String.valueOf(c.getTimeInMillis());
	}

	/**
	 * 根据时间获取时间戳 (秒)
	 */
	public static String getLongTime(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String s_date = format.format(date);
		String str = "";
		Date d_date = null;
		try {
			d_date = format.parse(s_date);
			str = (d_date.getTime() / 1000) + "";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
		}
		return str;
	}

	// 格式化时间为 如 1小时前
	// public static String GetDateStringMethod(long dateTime,Context context) {
	// TimeUtil sertime=new TimeUtil(context);//服务器正确时间
	// String time = "";
	// String newDateTime = CommonUtils.addZeroForNum(dateTime + "");
	// Date date = new Date();
	// Date enddate = new Date();
	// SimpleDateFormat smdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// // 小时开始的格式 大HH表示24小时制，hh12小时制
	// SimpleDateFormat smdfH = new SimpleDateFormat("HH:mm");
	// // 更具一串数字返回一个预订格式的日期
	// date.setTime(Long.parseLong(newDateTime));
	// enddate.setTime(Long.parseLong(CommonUtils.addZeroForNum(sertime.getTime()+"")));
	// // 开始比较，返回相应的时间字符
	// try {
	// Date start = smdf.parse(smdf.format(date));
	// Date end = smdf.parse(smdf.format(enddate));
	// long t = (end.getTime() - start.getTime()) / 1000;
	// if (t >= 60 && t < 3600) {
	// // 相差分钟的
	// time = t / 60 + "分钟前";
	// } else if (t >= 3600 && t < (3600 * 24)) {
	// // 相差小时的
	// time = t / 3600 + "小时前 ";
	// } else if (t >= (3600 * 24)) {
	// // 相差天的
	// if (t / (3600 * 24) == 1) {
	// time = "昨天" + smdfH.format(date);
	// // time="昨天："+smdfH.format(date);
	// } else {
	// time = smdf.format(date);
	// String mouth = time.substring(5, time.lastIndexOf("-"));
	// String day = time.substring(time.lastIndexOf("-") + 1,
	// time.indexOf(" "));
	// String daytime = time.substring(time.indexOf(" "),
	// time.lastIndexOf(":"));
	// time = mouth + "月" + day + "日" + daytime;
	// }
	//
	// } else {
	// // 相差秒的
	// time = "刚刚";
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// return time;
	// }
	// 格式化时间为 如 1小时前
	// public static String GetDateStringMethod(long dateTime, Context context)
	// {
	// TimeUtil sertime = new TimeUtil(context);// 服务器正确时间
	// String time1 = "";
	// String time2 = "";
	// String newDateTime = addZeroForNum(dateTime + "");
	// String endDateTime = addZeroForNum(sertime.getTime() + "");
	// SimpleDateFormat smdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// SimpleDateFormat smdf2 = new SimpleDateFormat("yyyy-MM-dd");
	// // 开始比较，返回相应的时间字符
	// try {
	// Date start = smdf.parse(getTimeByLong(newDateTime));
	// Date end = smdf.parse(getTimeByLong(endDateTime));
	// String s_start = smdf2.format(start);
	// String s_end = smdf2.format(end);
	// time1 = getTimeByLong(newDateTime);
	// time2 = getTimeByLong(endDateTime);
	// if (s_start.equals(s_end)) {
	// String daytime = time1.substring(time1.indexOf(" "),
	// time1.lastIndexOf(":"));
	// time1 = "今天 " + daytime;
	// } else if (isYearstaday(Long.parseLong(newDateTime),
	// Long.parseLong(endDateTime))) {
	// String daytime = time1.substring(time1.indexOf(" "),
	// time1.lastIndexOf(":"));
	// time1 = "昨天 " + daytime;
	// } else {
	// String year1 = time1.substring(0, 4);
	// String year2 = time2.substring(0, 4);
	// String mouth = time1.substring(5, time1.lastIndexOf("-"));
	// String day = time1.substring(time1.lastIndexOf("-") + 1,
	// time1.indexOf(" "));
	// String daytime = time1.substring(time1.indexOf(" "),
	// time1.lastIndexOf(":"));
	// if (year1.equals(year2)) {
	// time1 = Integer.parseInt(mouth) + "月" + day + "日 "
	// + daytime;
	// } else {
	// time1 = year1 + "/" + Integer.parseInt(mouth) + "/" + day;
	// // time1 = year1+"/"+Integer.parseInt(mouth) + "/" + day +
	// // " " + daytime;
	// }
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// return time1;
	// }

	/**
	 * 
	 * 1，非当年显示：XX年XX月XX日 XX:XX
	 * 
	 * 2，当年，非当天日期显示：XX月XX日 XX:XX
	 * 
	 * 3，当天：XX:XX
	 * 
	 * 4，在一分钟之类內的，只显示第一条 XX:XX
	 */
	// 格式化时间
	public static String GetDateStringMethodEx(long msgTime, long currentTime) {
		SimpleDateFormat years = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		SimpleDateFormat mounths = new SimpleDateFormat("MM月dd日 HH:mm");
		SimpleDateFormat hours = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		date.setTime(msgTime);
		if (isTaday(msgTime, currentTime))// 如果是今天
		{
			// 显示 08:08
			return hours.format(date);
		} else if (isYearstaday(msgTime, currentTime))// 如果是昨天
		{
			// 显示 几月几日
			return mounths.format(date);
		} else if (isYear(msgTime, currentTime))// 如果是今年
		{
			// 显示 几月几日
			return mounths.format(date);
		} else {
			// 显示 年 月 日
			return years.format(date);
		}

	}

	/**
	 * 判断日期是不是昨天（传入当前日期和需要判断的日期，得到昨天日期，在与需要判断的日期判断是否相等）
	 * 
	 * @param time
	 * @param currentTime
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static boolean isYearstaday(long time, long currentTime) {
		boolean isYearstaday = false;
		Date date = new Date();// 取时间
		Date date2 = new Date();
		date.setTime(currentTime);// 当前日期传进去
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE, -1);// 把日期往后增加一天.整数往后推,负数往前移动
		date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);// 得到昨天
		date2.setTime(time);
		String dateString2 = formatter.format(date2);// 当前时间

		// LogUtil.info("date", "昨天时间="+dateString);
		// LogUtil.info("date", "需要判断的时间="+dateString2);
		if (dateString.equals(dateString2)) {
			// LogUtil.info("date", "这个日期是昨天的");
			isYearstaday = true;
		}
		return isYearstaday;
	}

	/**
	 * 判断日期是不是今年
	 * 
	 * @param time
	 * @param currentTime
	 * @return
	 */
	public static boolean isYear(long time, long currentTime) {
		boolean isYear = false;
		Date date = new Date();// 取时间
		Date date2 = new Date();
		date.setTime(currentTime);// 当前日期传进去
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		String dateString = formatter.format(date);// 得到昨天
		date2.setTime(time);
		String dateString2 = formatter.format(date2);// 当前时间
		if (dateString.equals(dateString2)) {
			// LogUtil.info("date", "这个日期是今年的");
			isYear = true;
		} else {
			// LogUtil.info("date", "这个日期不是今年的");
		}
		return isYear;
	}

	/**
	 * 判断日期是不是今天
	 * 
	 * @param time
	 * @param currentTime
	 * @return
	 */
	public static boolean isTaday(long time, long currentTime) {
		boolean isTaday = false;
		Date date = new Date();// 取时间
		Date date2 = new Date();
		date.setTime(currentTime);// 当前日期传进去
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);// 得到昨天
		date2.setTime(time);
		String dateString2 = formatter.format(date2);// 当前时间
		// LogUtil.info("date", "今天时间="+dateString);
		// LogUtil.info("date", "需要判断的时间="+dateString2);
		if (dateString.equals(dateString2)) {
			// LogUtil.info("date", "这个日期是今天的");
			isTaday = true;
		}
		return isTaday;
	}

	public static String deleteMilliseconds(long date) {
		return String.valueOf(date / 1000);
	}

	/**
	 * 格式化时间
	 * 
	 * @param msgTime
	 * @return
	 */
	public static String GetDateStringMethod(long msgTime) {
		SimpleDateFormat mounths = new SimpleDateFormat("MM月dd日");
		Date date = new Date();
		date.setTime(Long.parseLong(addZeroForNum(msgTime + "")));
		return mounths.format(date);
	}

	/**
	 * 日期用，毫秒数不够位数在后面补上0
	 * 
	 * @param str
	 * @return
	 */
	public static String addZeroForNum(String str) {
		int strLen = str.length();
		StringBuffer sb = null;
		while (strLen < 13) {
			sb = new StringBuffer();
			// sb.append("0").append(str);// 左(前)补0
			sb.append(str).append("0");// 右(后)补0
			str = sb.toString();
			strLen = str.length();
		}
		return str;
	}
}
