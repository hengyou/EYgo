package com.fortunes.view;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.HorizontalScrollView;

/*
 * 自定义的 滚动控件
 * 重载�?onScrollChanged（滚动条变化�?监听每次的变化�?知给 观察(此变化的)观察�?
 * 可使�?AddOnScrollChangedListener 来订阅本控件�?滚动条变�?
 * */
public class MyHScrollView extends HorizontalScrollView {
	ScrollViewObserver mScrollViewObserver = new ScrollViewObserver();
	private GestureDetector mGestureDetector;

	public MyHScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mGestureDetector = new GestureDetector(new HScrollDetector()); 
	    setFadingEdgeLength(0); 
	}

	public MyHScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mGestureDetector = new GestureDetector(new HScrollDetector()); 
	    setFadingEdgeLength(0); 
	}

	public MyHScrollView(Context context) {
		super(context);
		mGestureDetector = new GestureDetector(new HScrollDetector());
		setFadingEdgeLength(0);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		return super.onTouchEvent(ev);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		/*
		 * 当滚动条移动后，引发 滚动事件。通知给观察者，观察者会传达给其他的�?
		 */
		if (mScrollViewObserver != null && (l != oldl || t != oldt)) {
			mScrollViewObserver.NotifyOnScrollChanged(l, t, oldl, oldt);

		}
		super.onScrollChanged(l, t, oldl, oldt);
	}

	/*
	 * 订阅 本控�?�?滚动条变化事�?
	 */
	public void AddOnScrollChangedListener(OnScrollChangedListener listener) {
		mScrollViewObserver.AddOnScrollChangedListener(listener);
	}
	
    @Override
  	 public boolean onInterceptTouchEvent(MotionEvent ev) { 
  	     return super.onInterceptTouchEvent(ev) && mGestureDetector.onTouchEvent(ev); 
  	 } 

	/*
	 * 取消 订阅 本控�?�?滚动条变化事�?
	 */
	public void RemoveOnScrollChangedListener(OnScrollChangedListener listener) {
		mScrollViewObserver.RemoveOnScrollChangedListener(listener);
	}

	/*
	 * 当发生了滚动事件�?
	 */
	public static interface OnScrollChangedListener {
		public void onScrollChanged(int l, int t, int oldl, int oldt);

	}

	/*
	 * 观察�?
	 */
	public static class ScrollViewObserver {
		List<OnScrollChangedListener> mList;

		public ScrollViewObserver() {
			super();
			mList = new ArrayList<OnScrollChangedListener>();
		}

		public void AddOnScrollChangedListener(OnScrollChangedListener listener) {
			mList.add(listener);
		}

		public void RemoveOnScrollChangedListener(
				OnScrollChangedListener listener) {
			mList.remove(listener);
		}

		public void NotifyOnScrollChanged(int l, int t, int oldl, int oldt) {
			if (mList == null || mList.size() == 0) {
				return;
			}
			for (int i = 0; i < mList.size(); i++) {
				if (mList.get(i) != null) {
					mList.get(i).onScrollChanged(l, t, oldl, oldt);
				}
			}
		}
	}

	// Return false if we're scrolling in the y direction
	class HScrollDetector extends SimpleOnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			if (Math.abs(distanceX) > Math.abs(distanceY)) {
				return true;
			}

			return false;
		}
	}
}
