package com.fortunes.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.EYgo.R;

/**
 * 如果不需要下拉刷新直接在canPullDown中返回false，这里的自动加载和下拉刷新没有冲突，通过增加在尾部的footerview实现自动加载，
 * 所以在使用中不要再动footerview了
 * 更多详解见博客http://blog.csdn.net/zhongkejingwang/article/details/38963177
 * 
 * @author chenjing
 * 
 */
@SuppressLint({ "InflateParams", "ClickableViewAccessibility" })
public class PullableListView extends ListView {
	public static final int INIT = 0;
	public static final int LOADING = 1;
	private OnLoadListener mOnLoadListener;
	private ProgressBar mLoadingView;
	private TextView mStateTextView;
	private int state = INIT;
	private boolean canLoad, unfinished = true;
	View view;

	public PullableListView(Context context) {
		super(context);
		init(context);
	}

	public PullableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public PullableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		view = LayoutInflater.from(context).inflate(R.layout.load_more, null);
		mLoadingView = (ProgressBar) view.findViewById(R.id.loading_icon);
		mStateTextView = (TextView) view.findViewById(R.id.loadstate_tv);

		// Animation animation = AnimationUtils.loadAnimation(context,
		// R.anim.loading_animation);
		// animation.setInterpolator(new LinearInterpolator());
		// mLoadingView.setAnimation(animation);
		addFooterView(view, null, false);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		switch (ev.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			// 按下的时候禁止自动加载
			canLoad = false;
			break;
		case MotionEvent.ACTION_UP:
			// 松开手判断是否自动加载
			canLoad = true;
			checkLoad();
			break;
		}
		return super.onTouchEvent(ev);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		// 在滚动中判断是否满足自动加载条件
		checkLoad();
	}

	/**
	 * 判断是否满足自动加载条件
	 */
	private void checkLoad() {
		if (reachBottom() && mOnLoadListener != null && state != LOADING && canLoad && unfinished) {
			mOnLoadListener.onLoad(this);
			changeState(LOADING);
		}
	}

	private void changeState(int state) {
		this.state = state;
		switch (state) {
		case INIT:
			mLoadingView.setVisibility(View.GONE);
			mStateTextView.setText(R.string.more);
			break;

		case LOADING:
			mLoadingView.setVisibility(View.VISIBLE);
			mStateTextView.setText(R.string.loading);
			break;
		}
	}

	/**
	 * 完成加载
	 */
	public void finishLoading() {
		changeState(INIT);
	}

	/**
	 * 去除底部加载dialog
	 */
	public void compelte() {
		unfinished = false;
		// mStateTextView.setVisibility(View.GONE);
		// mLoadingView.setVisibility(View.GONE);
		removeFooterView(view);
	}

	public void setOnLoadListener(OnLoadListener listener) {
		this.mOnLoadListener = listener;
	}

	/**
	 * @return footerview可见时返回true，否则返回false
	 */
	public boolean reachBottom() {
		if (getCount() == 0) {
			// 没有item的时候也可以上拉加载
			return true;
		} else if (getLastVisiblePosition() == (getCount() - 1)) {
			// 滑到底部了
			if (getChildAt(getLastVisiblePosition() - getFirstVisiblePosition()) != null
					&& getChildAt(getLastVisiblePosition() - getFirstVisiblePosition()).getTop() < getMeasuredHeight())
				return true;
		}
		return false;
	}

	public interface OnLoadListener {

		/**
		 * 监听是否滑动到底部
		 * 
		 * @param pullableListView
		 */
		void onLoad(PullableListView pullableListView);
	}
}
