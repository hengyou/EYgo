package com.fortunes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * @ClassName:[MyListView]
 * @Description:[自定义ListView，解决ScrollView中嵌套ListView显示不正常的问题（1行）]
 * @author liuyongzheng
 * @CreateDate:[2014-4-28 下午8:15:55]
 * @UpdateUser: UpdateUser
 * @UpdateDate: [2014-4-28 下午8:15:55]
 * @UpdateRemark: [说明本次修改内容]
 * @version [V1.0]
 */
public class MyListView extends ListView {

	public MyListView(Context context) {
		// TODO Auto-generated method stub
		super(context);
	}

	public MyListView(Context context, AttributeSet attrs) {
		// TODO Auto-generated method stub
		super(context, attrs);
	}

	public MyListView(Context context, AttributeSet attrs, int defStyle) {
		// TODO Auto-generated method stub
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}