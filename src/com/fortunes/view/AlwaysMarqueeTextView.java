package com.fortunes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @ClassName: AlwaysMarqueeTextView
 * @Description:跑马灯效果 (自动向左滚动)。需要在布局添加属性： android:ellipsize="marquee"
 *                    android:focusableInTouchMode="true"
 *                    android:marqueeRepeatLimit="marquee_forever"
 *                    android:textAppearance=
 *                    "?android:attr/textAppearanceMedium"
 *                    android:singleLine="true"
 * @author: JimmyWang
 * @date 2015年11月17日下午4:34:56
 */
public class AlwaysMarqueeTextView extends TextView {
	public AlwaysMarqueeTextView(Context context) {
		super(context);
	}

	public AlwaysMarqueeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AlwaysMarqueeTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean isFocused() {
		return true;
	}
}
