package com.fortunes.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.EYgo.R;

/**
 * @ClassName: KeyBoardDialog
 * @Description:秘码键盘Dialog
 */
public class KeyBoardDialog extends Dialog {
	Activity activity;
	private View view;
	private boolean isOutSideTouch = true;

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public boolean isOutSideTouch() {
		return isOutSideTouch;
	}

	public void setOutSideTouch(boolean isOutSideTouch) {
		this.isOutSideTouch = isOutSideTouch;
	}

	public KeyBoardDialog(Context context, int theme) {
		super(context, theme);
		// TODO Auto-generated constructor stub
	}

	public KeyBoardDialog(Context context) {
		this(context, 0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param activity
	 *            上下文
	 * @param view
	 *            视图
	 */
	public KeyBoardDialog(Activity activity, View view) {
		super(activity, R.style.MyDialog);
		this.activity = activity;
		this.view = view;
	}

	public KeyBoardDialog(Activity activity, View view, int theme) {
		super(activity, theme);
		this.activity = activity;
		this.view = view;
	}

	public KeyBoardDialog(Activity activity, View view, int theme, boolean isOutSide) {
		super(activity, theme);
		this.activity = activity;
		this.view = view;
		this.isOutSideTouch = isOutSide;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(view);
		setCanceledOnTouchOutside(isOutSideTouch);

		DisplayMetrics dm = new DisplayMetrics();
		// 取得窗口属性
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

		// 窗口的宽度
		int screenWidth = dm.widthPixels;
		int screenHeight = dm.heightPixels;
		WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
		layoutParams.width = screenWidth;
		layoutParams.height = screenHeight - 60;
		this.getWindow().setAttributes(layoutParams);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {// 返回键操作
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			appExit();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 返回键操作
	 */
	private void appExit() {

		NotiDialog dialog = new NotiDialog(activity, "是否取消交易");
		dialog.show();
		dialog.setTitleStr("温馨提示");
		dialog.setOkButtonText("确认");
		dialog.setPositiveListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				KeyBoardDialog.this.dismiss();
			}
		}).setNegativeListener(null);
	}
}
