package com.fortunes.util;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Layout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * @author TextView工具类
 *
 */
public class TextViewUtils {

	

	/**
	 * TextVeiw文本内容是否超过行数
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isOver(TextView tv) {
		Layout l = tv.getLayout();
		if (l != null) {
			int lines = l.getLineCount();// 获取text在该布局的行数
			if (lines > 0) {
				if (l.getEllipsisCount(lines - 1) > 0) {// getEllipsisCount()方法返回超出的字符数量,(lines
														// - 1)表示获取最后一行ps:下标从0开始

					return true;
				}

			}

		}
		return false;
	}

	/**
	 * item的dialog显示
	 * 
	 * @param name
	 */
	public static void showDialog(Context context, String name) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(name);
		builder.setCancelable(true);
		builder.create().show();
	}
	
	
	/**
	 *  监听TextVeiw文本内容是否超过行数
	 *
	 */
	public static class TextViewIsOverListener implements OnClickListener {
		Context context;
		TextView tv;

		public TextViewIsOverListener(Context context, TextView tv) {
			super();
			this.tv = tv;
			this.context = context;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (TextViewUtils.isOver(tv)) {
				TextViewUtils.showDialog(context, tv.getText().toString());
			}
		}
	}

}
