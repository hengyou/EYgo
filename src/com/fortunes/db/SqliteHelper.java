package com.fortunes.db;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.fortunes.config.AppConfig;
import com.fortunes.db.bean.AddressInfo;
import com.fortunes.db.bean.ShopDBBean;
import com.fortunes.util.LogUtils;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * 
 * 数据库工具类
 * 
 */
public class SqliteHelper extends OrmLiteSqliteOpenHelper {
	private static final String TAG = SqliteHelper.class.getSimpleName();

	private static final String DATABASE_NAME = AppConfig.DB_NAME;// 数据库名字
	private static final int DATABASE_VERSION = AppConfig.DB_VERSION;// 数据库的版本号

	private Dao<ShopDBBean, Integer> shopDao = null;// 购物车
	private Dao<AddressInfo, Integer> addressDao = null;// 收货地址

	public SqliteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(connectionSource, ShopDBBean.class);
			TableUtils.createTable(connectionSource, AddressInfo.class);
		} catch (java.sql.SQLException e) {
			LogUtils.e(TAG, "创建数据库失败");
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {

			TableUtils.dropTable(connectionSource, ShopDBBean.class, true);
			TableUtils.createTable(connectionSource, ShopDBBean.class);
			TableUtils.dropTable(connectionSource, AddressInfo.class, true);
			TableUtils.createTable(connectionSource, AddressInfo.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		super.close();
	}

	/**
	 * 用于记录购物车
	 * 
	 * @return
	 */
	public Dao<ShopDBBean, Integer> getShopDao() {
		if (shopDao == null) {
			try {
				shopDao = getDao(ShopDBBean.class);
			} catch (java.sql.SQLException e) {
				e.printStackTrace();
			}
		}
		return shopDao;
	}

	/**
	 * 用于记录收货地址
	 * 
	 * @return
	 */
	public Dao<AddressInfo, Integer> getAddressDao() {
		if (addressDao == null) {
			try {
				addressDao = getDao(AddressInfo.class);
			} catch (java.sql.SQLException e) {
				e.printStackTrace();
			}
		}
		return addressDao;
	}

}
