package com.fortunes.db;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.fortunes.db.bean.ShopDBBean;
import com.j256.ormlite.dao.Dao;

/**
 * @author gongchaobin
 * 
 *         用户身份表
 * 
 * @version 2013-7-16
 * 
 */
public class ShopDao {
	private Dao shopDao;
	private SqliteHelper helper;
	// private PreferencesHelper mHelper;

	public static final int NOT_CURRENT_STATUS = 0;
	public static final int IS_CURRENT_STATUS = 1;

	public static ShopDao singleton;

	public ShopDao() {
	}

	public ShopDao(Context context) {
		helper = new SqliteHelper(context.getApplicationContext());
		shopDao = helper.getShopDao();
	}

	public static ShopDao getInstance(Context context) {
		if (singleton == null) {
			synchronized (ShopDao.class) {
				singleton = new ShopDao(context);
				System.out.println("创建");
			}
		}
		return singleton;
	}

	/** 插入数据 **/
	public void insertData(ShopDBBean phoneNumber) {
		try {
			List<ShopDBBean> list = shopDao.queryForAll();
			boolean bool = false;

			// if (list.size() == 0) {
			shopDao.create(phoneNumber);
			// } else {
			// for (ShopDBBean number : list) {
			// if (number.getId().equals(phoneNumber.getId())) {
			// bool = true;
			// break;
			// }
			// }
			//
			// if (!bool) {
			// shopDao.create(phoneNumber);
			// }
			// }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** 根据身份ID查找数据 **/
	public ShopDBBean queryDataById(String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		List<ShopDBBean> list = null;

		try {
			list = shopDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/** 根据身份ID查找数据 **/
	public ShopDBBean queryDataByIdAndPharName(String id, String pharName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		map.put("pharName", pharName);
		List<ShopDBBean> list = null;

		try {
			list = shopDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<ShopDBBean> listById(String id) {
		List<ShopDBBean> list = null;
		try {
			list = shopDao.queryBuilder().where().eq("IdentityId", id).query();
			if (list != null && list.size() != 0) {
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public ShopDBBean listById(String id, String pharName) {
		List<ShopDBBean> list = null;
		try {
			list = shopDao.queryBuilder().where().eq("IdentityId", id).and().eq("pharName", pharName).query();
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public ShopDBBean listByPharName(String pharName) {
		List<ShopDBBean> list = null;
		try {
			list = shopDao.queryBuilder().where().eq("pharName", pharName).query();
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 根据身份ID批量查找数据 **/
	public List<ShopDBBean> queryDataListById(String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		List<ShopDBBean> list = null;
		try {
			list = shopDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/** 查找所有数据 **/
	public List<ShopDBBean> queryForAll() {
		try {
			return shopDao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 更新数量 **/
	public void updateData(int count, String id) {
		ShopDBBean number = queryDataById(id);
		if (number != null) {
			number.setCount(count);
			try {
				shopDao.update(number);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/** 更新数量 **/
	public void updateData(ShopDBBean number) {
		try {
			shopDao.update(number);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** 清除所有数据 **/
	public void delAll() {
		try {
			List<ShopDBBean> ps = shopDao.queryForAll();
			if (null != ps) {
				shopDao.delete(ps);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/** 清除一条数据 **/
	public int delData(ShopDBBean bean) {
		try {
			if (null != bean) {
				return shopDao.delete(bean);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
