﻿package com.fortunes.db.bean;

/**
 * @ClassName: RegionBean
 * @Description:地区管理
 * @author: JimmyWang
 * @date 2015年11月5日下午3:32:25
 */
public class RegionBean {

	private String name;
	private String id;
	private String parent_id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
}
