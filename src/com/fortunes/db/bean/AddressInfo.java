package com.fortunes.db.bean;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

/**
 * @ClassName: AddressInfo
 * @Description:收货地址信息 数据类
 * @author: JimmyWang
 * @date 2015年11月5日下午3:30:52
 */
@SuppressWarnings("serial")
public class AddressInfo implements Serializable {
	@DatabaseField(generatedId = true, unique = true)
	int id;

	@DatabaseField
	private String date;
	@DatabaseField
	private String name;
	@DatabaseField
	private String phone;
	@DatabaseField
	private String provinces;
	@DatabaseField
	private String street;
	@DatabaseField
	private String postcode;
	@DatabaseField
	private boolean status = false;
	@DatabaseField
	private boolean selected = false;

	public AddressInfo() {
		super();
	}

	public AddressInfo(String date, String name, String phone, String provinces, String street, String postcode,
			boolean status, boolean selected) {
		super();
		this.date = date;
		this.name = name;
		this.phone = phone;
		this.provinces = provinces;
		this.street = street;
		this.postcode = postcode;
		this.status = status;
		this.selected = selected;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProvinces() {
		return provinces;
	}

	public void setProvinces(String provinces) {
		this.provinces = provinces;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "AddressInfo [dataid=" + date + ", name=" + name + ", phone=" + phone + ", provinces=" + provinces
				+ ", street=" + street + ", postcode=" + postcode + ", status=" + status + ", selected=" + selected
				+ "]";
	}

}
