package com.fortunes.db.bean;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

public class ShopDBBean implements Serializable {
	@DatabaseField(generatedId = true, unique = true)
	int id;

	@DatabaseField
	public String IdentityId; // 身份ID
	@DatabaseField
	private String Username;
	@DatabaseField
	private String pharName;
	@DatabaseField
	private double pharPrice;
	@DatabaseField
	private String isYiBao;
	@DatabaseField
	private String pharImg;
	@DatabaseField
	private String pharType;
	@DatabaseField
	private int count;// 购买数量

	public ShopDBBean() {
		super();
	}

	public ShopDBBean(String identityId, String username, String pharName,
			double pharPrice, String isYiBao, String pharImg, String pharType,
			int count) {
		super();
		IdentityId = identityId;
		Username = username;
		this.pharName = pharName;
		this.pharPrice = pharPrice;
		this.isYiBao = isYiBao;
		this.pharImg = pharImg;
		this.pharType = pharType;
		this.count = count;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getIdentityId() {
		return IdentityId;
	}

	public void setIdentityId(String identityId) {
		IdentityId = identityId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getPharName() {
		return pharName;
	}

	public void setPharName(String pharName) {
		this.pharName = pharName;
	}

	public double getPharPrice() {
		return pharPrice;
	}

	public void setPharPrice(double pharPrice) {
		this.pharPrice = pharPrice;
	}

	public String getIsYiBao() {
		return isYiBao;
	}

	public void setIsYiBao(String isYiBao) {
		this.isYiBao = isYiBao;
	}

	public String getPharImg() {
		return pharImg;
	}

	public void setPharImg(String pharImg) {
		this.pharImg = pharImg;
	}

	public String getPharType() {
		return pharType;
	}

	public void setPharType(String pharType) {
		this.pharType = pharType;
	}

	@Override
	public String toString() {
		return "ShopDBBean [IdentityId=" + IdentityId + ", Username="
				+ Username + ", pharName=" + pharName + ", pharPrice="
				+ pharPrice + ", isYiBao=" + isYiBao + ", pharImg=" + pharImg
				+ ", pharType=" + pharType + ", count=" + count + "]";
	}

}
