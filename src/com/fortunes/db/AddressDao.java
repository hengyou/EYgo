package com.fortunes.db;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.fortunes.db.bean.AddressInfo;
import com.fortunes.db.bean.ShopDBBean;
import com.fortunes.util.LogUtils;
import com.j256.ormlite.dao.Dao;

/**
 * @ClassName: AddressDao
 * @Description:收货地址表
 * @author: JimmyWang
 * @date 2015年11月12日上午9:41:48
 */
public class AddressDao {
	@SuppressWarnings("rawtypes")
	private Dao addressDao;
	private SqliteHelper helper;
	// private PreferencesHelper mHelper;

	public static final int NOT_CURRENT_STATUS = 0;
	public static final int IS_CURRENT_STATUS = 1;

	public static AddressDao singleton;

	public AddressDao() {
	}

	public AddressDao(Context context) {
		helper = new SqliteHelper(context.getApplicationContext());
		addressDao = helper.getAddressDao();
	}

	public static AddressDao getInstance(Context context) {
		if (singleton == null) {
			synchronized (AddressDao.class) {
				singleton = new AddressDao(context);
				System.out.println("创建");
			}
		}
		return singleton;
	}

	/**
	 * 插入一条数据
	 * 
	 * @param address
	 * @return 返回true为插入数据成功
	 */
	public boolean insertAddress(AddressInfo address) {
		try {
			if (addressDao.create(address) > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return false;
	}

	/** 根据身份ID查找数据 **/
	public ShopDBBean queryDataById(String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		List<ShopDBBean> list = null;

		try {
			list = addressDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/** 根据身份ID查找数据 **/
	public ShopDBBean queryDataByIdAndPharName(String id, String pharName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		map.put("pharName", pharName);
		List<ShopDBBean> list = null;

		try {
			list = addressDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<ShopDBBean> listById(String id) {
		List<ShopDBBean> list = null;
		try {
			list = addressDao.queryBuilder().where().eq("IdentityId", id).query();
			if (list != null && list.size() != 0) {
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public ShopDBBean listById(String id, String pharName) {
		List<ShopDBBean> list = null;
		try {
			list = addressDao.queryBuilder().where().eq("IdentityId", id).and().eq("pharName", pharName).query();
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过身份证号获取所有购物车中的药品信息
	 * 
	 * @param userId
	 * @return
	 */
	public ShopDBBean listByPharName(String pharName) {
		List<ShopDBBean> list = null;
		try {
			list = addressDao.queryBuilder().where().eq("pharName", pharName).query();
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 根据身份ID批量查找数据 **/
	public List<ShopDBBean> queryDataListById(String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", id);
		List<ShopDBBean> list = null;
		try {
			list = addressDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @return 收货地址列表(所有数据)
	 */
	public List<AddressInfo> queryAddress() {
		try {
			return addressDao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 更新数量 **/
	public void updateData(int count, String id) {
		ShopDBBean number = queryDataById(id);
		if (number != null) {
			number.setCount(count);
			try {
				addressDao.update(number);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/** 更新数量 **/
	public void updateData(ShopDBBean number) {
		try {
			addressDao.update(number);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** 清除所有数据 **/
	public void delAll() {
		try {
			List<AddressInfo> ps = addressDao.queryForAll();
			if (null != ps) {
				addressDao.delete(ps);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除单一地址
	 * 
	 * @param bean
	 *            (一条地址)
	 * @return 删除成功返回true
	 */
	public boolean delOneAddress(AddressInfo bean) {
		try {
			if (null != bean) {
				if (addressDao.delete(bean) > 0) {
					return true;
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 修改地址
	 * 
	 * @param address
	 *            (更改的地址)
	 * @return 返回true为修改成功
	 */
	public boolean updateAddress(AddressInfo address) {

		try {
			if (address != null) {
				AddressInfo newAddress = queryDataByDate(address.getDate());
				newAddress.setDate(address.getDate());
				newAddress.setName(address.getName());
				newAddress.setPhone(address.getPhone());
				newAddress.setPostcode(address.getPostcode());
				newAddress.setProvinces(address.getProvinces());
				newAddress.setSelected(address.isSelected());
				newAddress.setStatus(address.isStatus());
				newAddress.setStreet(address.getStreet());
				int results = addressDao.update(newAddress);
				if (results > 0) {
					return true;
				}
			} else {
				LogUtils.i(">>>>", "address=: is null");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 修改地址
	 * 
	 * @param address
	 *            (更改的地址)
	 * @return 返回true为修改成功
	 */
	public boolean update(AddressInfo address) {

		try {
			AddressInfo newAddress = null;
			List<AddressInfo> list = addressDao.queryBuilder().where().eq("pharName", address.getDate()).query();
			if (list != null && list.size() > 0) {

			}

			if (address != null) {
				int results = addressDao.update(address);
				LogUtils.i(">>>>", "updateAddress=:" + results);
				if (results > 0) {
					return true;
				}
			} else {
				LogUtils.i(">>>>", "address=: is null");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * 重置默认(改变所有状态为不默认)
	 */
	public void resetStatus() {

		try {
			List<AddressInfo> list = addressDao.queryForAll();
			if (list != null && list.size() > 0) {
				Iterator<AddressInfo> iterator = list.iterator();
				while (iterator.hasNext()) {
					AddressInfo a = iterator.next();
					a.setStatus(false);
					addressDao.update(a);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 重置选中项(改变所有状态为不选中)
	 */
	public void resetSelected() {

		try {
			List<AddressInfo> list = addressDao.queryForAll();
			if (list != null && list.size() > 0) {
				Iterator<AddressInfo> iterator = list.iterator();
				while (iterator.hasNext()) {
					AddressInfo a = iterator.next();
					a.setSelected(false);
					addressDao.update(a);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/** 根据日期查找数据 **/
	public AddressInfo queryDataByDate(String date) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("date", date);
		List<AddressInfo> list = null;

		try {
			list = addressDao.queryForFieldValues(map);
			if (list != null && list.size() != 0) {
				return list.get(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
