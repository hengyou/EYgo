package com.fortunes.ui.fragment.viewpager_fg;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.EYgo.R;

/**
 * @ClassName: ImageFragment
 * @Description:主界面广告栏 Fragment
 */
public class ImageFragment extends Fragment {

	private View layoutView;
	private ImageView image;
	@SuppressWarnings("unused")
	private Activity mActivity;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (Activity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Bundle bundle = getArguments();
		if (layoutView == null) {
			layoutView = inflater.inflate(R.layout.fragment_viewpager, null, false);

		}
		image = (ImageView) layoutView.findViewById(R.id.viewpager_image);
		if (bundle != null) {
			image.setImageResource(bundle.getInt("image"));
		}
		// image.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// ActivityTools.skipActivity(mActivity, InfoActivity.class);
		// }
		// });
		return layoutView;
	}
}
