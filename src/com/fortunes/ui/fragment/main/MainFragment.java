package com.fortunes.ui.fragment.main;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.EYgo.R;
import com.fortunes.base.BaseFragment;

/**
 * @ClassName: MainFragment
 * @Description:主界面(首页)
 */
public class MainFragment extends BaseFragment {
	private static final String TAG = MainFragment.class.getSimpleName();
	private Activity mActivity;
	private View layoutView;

	/** 顶部标题栏的左边按钮 */
	private Button mLeftIconBtn;

	/** 显示标题 */
	private TextView toptext;



	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (Activity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mActivity = (Activity) getActivity();
		if (layoutView == null) {
			layoutView = inflater.inflate(R.layout.fragment_main, container,
					false); // 加载fragment布局
			findViewById();
			setListener();
			init();
		}
		// 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
		ViewGroup parent = (ViewGroup) layoutView.getParent();
		if (parent != null) {
			parent.removeView(layoutView);
		}
		return layoutView;
	}

	@Override
	protected void findViewById() {
		// TODO Auto-generated method stub
		mLeftIconBtn = (Button) layoutView.findViewById(R.id.top_bar_left_btn);
		toptext = (TextView) layoutView.findViewById(R.id.top_bar_titleTv);
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		mLeftIconBtn.setVisibility(View.GONE);
		toptext.setText(getString(R.string.main));
	}



}
