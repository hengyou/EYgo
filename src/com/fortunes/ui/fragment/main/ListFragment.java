package com.fortunes.ui.fragment.main;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.EYgo.R;
import com.fortunes.base.BaseFragment;

/**
 * @ClassName: ListFragment
 * @Description:清单界面
 */
public class ListFragment extends BaseFragment {
	private static final String TAG = ListFragment.class.getSimpleName();
	private Activity mActivity;
	private View layoutView;
	/** 顶部标题栏的左边按钮 */
	private Button mLeftIconBtn;
	/** 显示标题 */
	private TextView toptext;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (Activity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		mActivity = (Activity) getActivity();
		if (layoutView == null) {
			layoutView = inflater.inflate(R.layout.fragment_list, container, false); // 加载fragment布局
			findViewById();
			init();
			setListener();
		}
		// 缓存的rootView需要判断是否已经被加过parent，如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
		ViewGroup parent = (ViewGroup) layoutView.getParent();
		if (parent != null) {
			parent.removeView(layoutView);
		}
		return layoutView;
	}

	@Override
	protected void findViewById() {
		mLeftIconBtn = (Button) layoutView.findViewById(R.id.top_bar_left_btn);
		toptext = (TextView) layoutView.findViewById(R.id.top_bar_titleTv);
	}

	@Override
	protected void setListener() {
	
	}

	@Override
	protected void init() {
		mLeftIconBtn.setVisibility(View.GONE);
		toptext.setText(getString(R.string.list));
		
	}



}
