package com.fortunes.ui.bean.main;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: OrderDataBean
 * @Description:订单信息集合
 */
@SuppressWarnings("serial")
public class OrderDataBean extends BaseStateBean implements Serializable {
	private SingleDataStateBase singleData;// 记录
	private List<OrderBean> data;// 订单信息 集合

	public SingleDataStateBase getSingleData() {
		return singleData;
	}

	public void setSingleData(SingleDataStateBase singleData) {
		this.singleData = singleData;
	}

	public List<OrderBean> getData() {
		return data;
	}

	public void setData(List<OrderBean> data) {
		this.data = data;
	}

}
