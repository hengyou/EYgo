package com.fortunes.ui.bean.main;

/**
 * @ClassName: UserRegisterDataBean
 * @Description:用户注册
 */
public class UserRegisterDataBean extends BaseBean {
	String name;
	String mobilePhone;
	String idNo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

}
