package com.fortunes.ui.bean.main;

import java.io.Serializable;

/**
 * @ClassName: SingleDataBase
 * @Description:被作为singleData，带有MSG、ROWCOUNT两个参数
 */
@SuppressWarnings("serial")
public class SingleDataBase implements Serializable {
	private String MSG;
	private String ROWCOUNT;

	public String getMSG() {
		return MSG;
	}

	public void setMSG(String mSG) {
		MSG = mSG;
	}

	public String getROWCOUNT() {
		return ROWCOUNT;
	}

	public void setROWCOUNT(String rOWCOUNT) {
		ROWCOUNT = rOWCOUNT;
	}

}
