package com.fortunes.ui.bean.main;

/**
 * @ClassName: BaseBean
 * @Description:所有Bean文件的基类
 */
public class BaseBean {
	private String MSG;

	public String getMSG() {
		return MSG;
	}

	public void setMSG(String mSG) {
		MSG = mSG;
	}
}
