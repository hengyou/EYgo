package com.fortunes.ui.bean.main;

/**
 * @ClassName: CheckVersionResDataBean
 * @Description:检查版本更新
 */
public class CheckVersionResDataBean extends BaseStateBean {
	String apkVersion;
	String msg;

	public String getApkVersion() {
		return apkVersion;
	}

	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
