package com.fortunes.ui.bean.main;

import java.io.Serializable;

/**
 * @ClassName: SingleDataStateBase
 * @Description:只带有参数ROWCOUNT(记录数)的实体类
 */
@SuppressWarnings("serial")
public class SingleDataStateBase implements Serializable {

	private String ROWCOUNT;// 记录数

	public String getROWCOUNT() {
		return ROWCOUNT;
	}

	public void setROWCOUNT(String rOWCOUNT) {
		ROWCOUNT = rOWCOUNT;
	}

}
