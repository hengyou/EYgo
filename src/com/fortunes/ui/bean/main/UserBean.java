package com.fortunes.ui.bean.main;

/**
 * @ClassName: UserBean
 * @Description:用户信息
 */
public class UserBean extends BaseStateBean{
	UserDataBean singleData;

	public UserDataBean getSingleData() {
		return singleData;
	}

	public void setSingleData(UserDataBean singleData) {
		this.singleData = singleData;
	}

}
