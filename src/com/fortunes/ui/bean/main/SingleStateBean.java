package com.fortunes.ui.bean.main;

/**
 * @ClassName: SingleStateBean
 * @Description:只带有state参数的实体类
 */
public class SingleStateBean {
	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
