package com.fortunes.ui.bean.main;

public class UserSingleBean extends BaseStateBean{
	BaseBean singleData;

	public BaseBean getSingleData() {
		return singleData;
	}

	public void setSingleData(BaseBean singleData) {
		this.singleData = singleData;
	}

}
