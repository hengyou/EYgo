package com.fortunes.ui.bean.main;

/**
 * @ClassName: BaseStateBean
 * @Description:带有状态state的Bean文件基类
 */
public class BaseStateBean {
	private String state;
	private String state_qz;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState_qz() {
		return state_qz;
	}

	public void setState_qz(String state_qz) {
		this.state_qz = state_qz;
	}

}
