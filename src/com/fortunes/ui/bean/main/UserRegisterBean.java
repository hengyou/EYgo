package com.fortunes.ui.bean.main;

/**
 * @ClassName: UserRegisterBean
 * @Description:用户注册
 */
public class UserRegisterBean extends BaseStateBean {
	UserRegisterDataBean singleData;

	public UserRegisterDataBean getSingleData() {
		return singleData;
	}

	public void setSingleData(UserRegisterDataBean singleData) {
		this.singleData = singleData;
	}

}
