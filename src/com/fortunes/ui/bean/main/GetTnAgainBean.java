package com.fortunes.ui.bean.main;

import java.io.Serializable;

/**
 * @ClassName: GetTnAgainBean
 * @Description:重新请求tn
 */
@SuppressWarnings("serial")
public class GetTnAgainBean implements Serializable{
	private String tn;
	private String state;

	public String getTn() {
		return tn;
	}

	public void setTn(String tn) {
		this.tn = tn;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
