package com.fortunes.ui.bean.main;

import java.io.Serializable;

/**
 * @ClassName: InfoTitle
 * @Description:信息界面 列表实体类
 */
@SuppressWarnings("serial")
public class InfoTitle implements Serializable {

	private String context;
	private int imgId;

	public InfoTitle() {
		super();
	}

	public InfoTitle(String context, int imgId) {
		super();
		this.context = context;
		this.imgId = imgId;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public int getImgId() {
		return imgId;
	}

	public void setImgId(int imgId) {
		this.imgId = imgId;
	}

}
