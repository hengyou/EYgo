package com.fortunes.ui.bean.main;

import java.io.Serializable;

/**
 * @ClassName: OrderBean
 * @Description:订单信息
 */
@SuppressWarnings("serial")
public class OrderBean implements Serializable {
	private String id;// 订单号
	private String tn;// 流水号
	private String type;// 参保类型
	private String city;// 参保城市
	private String baseNum;// 社保基数
	private String time;// 缴纳时间段
	private String total;// 缴费总金额
	private String endow;// 养老保险
	private String medic;// 医疗保险
	private String bear;// 生育保险
	private String createtime;// 订单创建时间
	private String payStatus;// 支付状态(0:未支付2:支付成功 3:支付失败4：已取消，5：已删除)

	/**
	 * @return the 订单号
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param 订单号
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the 流水号
	 */
	public String getTn() {
		return tn;
	}

	/**
	 * @param 流水号
	 *            the tn to set
	 */
	public void setTn(String tn) {
		this.tn = tn;
	}

	/**
	 * @return the 参保类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param 参保类型
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the 参保城市
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param 参保城市
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the 社保基数
	 */
	public String getBaseNum() {
		return baseNum;
	}

	/**
	 * @param 社保基数
	 *            the baseNum to set
	 */
	public void setBaseNum(String baseNum) {
		this.baseNum = baseNum;
	}

	/**
	 * @return the 缴纳时间段
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param 缴纳时间段
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the 缴费总金额
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param 缴费总金额
	 *            the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return the 养老保险
	 */
	public String getEndow() {
		return endow;
	}

	/**
	 * @param 养老保险
	 *            the endow to set
	 */
	public void setEndow(String endow) {
		this.endow = endow;
	}

	/**
	 * @return the 医疗保险
	 */
	public String getMedic() {
		return medic;
	}

	/**
	 * @param 医疗保险
	 *            the medic to set
	 */
	public void setMedic(String medic) {
		this.medic = medic;
	}

	/**
	 * @return the 生育保险
	 */
	public String getBear() {
		return bear;
	}

	/**
	 * @param 生育保险
	 *            the bear to set
	 */
	public void setBear(String bear) {
		this.bear = bear;
	}

	/**
	 * @return the 订单创建时间
	 */
	public String getCreatetime() {
		return createtime;
	}

	/**
	 * @param 订单创建时间
	 *            the createtime to set
	 */
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	/**
	 * @return the 支付状态
	 */
	public String getPayStatus() {
		return payStatus;
	}

	/**
	 * @param 支付状态
	 *            the payStatus to set
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

}
