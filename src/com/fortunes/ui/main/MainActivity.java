package com.fortunes.ui.main;

import java.net.SocketTimeoutException;


import org.apache.http.Header;
import org.apache.http.conn.ConnectTimeoutException;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.EYgo.R;
import com.fortunes.base.AppManager;
import com.fortunes.base.AppManager.MainChangeLayoutListener;
import com.fortunes.base.BaseFragmentActivity;
import com.fortunes.config.AppConfig;
import com.fortunes.http.Urls;
import com.fortunes.http.https;
import com.fortunes.key.KeyUtils.RequestKeyListener;
import com.fortunes.util.LogUtils;
import com.fortunes.util.PreferencesUtils;
import com.fortunes.util.StringUtils;
import com.fortunes.util.ToastUtils;
import com.fortunes.view.NotiDialog;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.umeng.analytics.MobclickAgent;

public class MainActivity extends BaseFragmentActivity implements OnClickListener {
	private static final String TAG = MainActivity.class.getSimpleName();
	// 底部tab控件
	private LinearLayout sblayout;
	private LinearLayout infolayout;
	private LinearLayout findlayout;
	private LinearLayout userinfolayout;
	//
	public static Fragment[] mFragments;
	public static int curentlay = 0;
	private String dataString = null;// 安全控件返回的数据
	private LocalBroadcastManager mLocalBroadcastManager;

	private int changlayout = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById();
		init();
		setListener();
	}

	@Override
	protected void findViewById() {
		// TODO Auto-generated method stub
		sblayout = (LinearLayout) findViewById(R.id.sblayout);
		infolayout = (LinearLayout) findViewById(R.id.infolayout);
		findlayout = (LinearLayout) findViewById(R.id.findlayout);
		userinfolayout = (LinearLayout) findViewById(R.id.userinfolayout);

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		sblayout.setOnClickListener(this);
		infolayout.setOnClickListener(this);
		findlayout.setOnClickListener(this);
		userinfolayout.setOnClickListener(this);
		keyUtils.setOnRequestKeyListener(new RequestKeyListener() {

			@Override
			public void callBack(String go) {
				// TODO Auto-generated method stub
				if (StringUtils.isEmpty(dataString)) {
					return;
				}
			}
		});
		AppManager.getAppManager().setMainChangeLayoutListener(// 关闭所有界面跳回首页选择哪个Fragment监听
				new MainChangeLayoutListener() {

					@Override
					public void callBack(int number) {
						// TODO Auto-generated method stub
						changlayout = number;
					}
				});
	}

	@Override
	protected void init() {
		setFragmentIndicator();
		currentTabByTag(0);// 程序第一次启动的时候调用一次设置显示的方法，只在启动的时候才会执行此句
	}


	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart(TAG);
		MobclickAgent.onResume(this);
		if (changlayout != -1) {// 绕过ChangeLayout.commit()异常
			currentTabByTag(changlayout);
			changlayout = -1;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd(TAG);
		MobclickAgent.onPause(this);
	}

	/**
	 * 实例化Fragment的方法
	 */
	private void setFragmentIndicator() {
		mFragments = new Fragment[5];
		mFragments[0] = getSupportFragmentManager().findFragmentById(R.id.SbFragment);
		mFragments[1] = getSupportFragmentManager().findFragmentById(R.id.InfoFragment);
		mFragments[2] = getSupportFragmentManager().findFragmentById(R.id.FindFragment);
		mFragments[3] = getSupportFragmentManager().findFragmentById(R.id.UserinfoFragment);
		findViewById(R.id.UserinfoFragment).setSelected(true);
	}

	/**
	 * 设置显示选中的Fragment的方法，隐藏所有未选中的Fragment
	 */
	private void ChangeLayout() {
		getSupportFragmentManager().beginTransaction().hide(mFragments[0]).hide(mFragments[1]).hide(mFragments[2])
				.hide(mFragments[3]).show(mFragments[curentlay]).commit();
	}

	/**
	 * @Description:[设置当前选中位置]
	 * @param index
	 */
	public void currentTabByTag(int index) {
		curentlay = index;
		sblayout.setSelected(false);
		infolayout.setSelected(false);
		findlayout.setSelected(false);
		userinfolayout.setSelected(false);
		switch (curentlay) {
		case 0:
			sblayout.setSelected(true);
			break;
		case 1:
			infolayout.setSelected(true);
			break;
		case 2:
			findlayout.setSelected(true);
			break;
		case 3:
			userinfolayout.setSelected(true);
			break;
		default:
			break;
		}
		ChangeLayout();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		sblayout.setSelected(false);
		infolayout.setSelected(false);
		findlayout.setSelected(false);
		userinfolayout.setSelected(false);
		switch (v.getId()) {
		case R.id.sblayout:
			curentlay = 0;
			sblayout.setSelected(true);
			ChangeLayout();// 调用设置显示的方法
			break;
		case R.id.infolayout:
			curentlay = 1;
			infolayout.setSelected(true);
			ChangeLayout();// 调用设置显示的方法
			break;
		case R.id.findlayout:
			curentlay = 2;
			findlayout.setSelected(true);
			ChangeLayout();// 调用设置显示的方法
			break;
		case R.id.userinfolayout:
			curentlay = 3;
			userinfolayout.setSelected(true);
			ChangeLayout();// 调用设置显示的方法
			break;
		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			if (curentlay == 0) {// 首页时候才会触发退出事件
				appExit();
			} else {
				curentlay = 0;
				currentTabByTag(curentlay);
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}


	
	// 退出程序
	private void appExit() {
		final NotiDialog dialog = new NotiDialog(this, "是否退出应用程序");
		dialog.show();
		dialog.setTitleStr("温馨提示");
		dialog.setOkButtonText("确认");
		dialog.setPositiveListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (PreferencesUtils.islogin(MainActivity.this)) {
					LogoutRequest();
				}
				mhandler.sendEmptyMessage(0);
			}
		}).setNegativeListener(null);
	}

	Handler mhandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			// 注销并清除状态
			PreferencesUtils.clearUserData(MainActivity.this);// 清除用户信息
			finish();
		}

	};

	private void LogoutRequest() {// 注销请求
		RequestParams rp = new RequestParams();
		rp.put("id", PreferencesUtils.getString(this, AppConfig.USER_ID));
		rp.put("code", PreferencesUtils.getString(this, AppConfig.USER_CODE));
		https.get(this, Urls.LOGOUT_URL, rp, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				if (!StringUtils.isEmpty(response)) {
					LogUtils.i(":response=", response);
				}
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				if (arg3 instanceof ConnectTimeoutException || arg3 instanceof SocketTimeoutException) {
					ToastUtils.showShortToast(getApplicationContext(),
							MainActivity.this.getString(R.string.network_timeout));
				} else {
					ToastUtils.showShortToast(getApplicationContext(),
							MainActivity.this.getString(R.string.network_error));
				}
				if (loadingDialog != null && loadingDialog.isShowing()) {
					dismissProgressDialog();
				}
			}
		});
	}
}
