package com.fortunes.ui.main;

import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.EYgo.R;
import com.fortunes.base.BaseActivity;
import com.fortunes.key.KeyUtils;
import com.fortunes.util.PreferencesUtils;

/**
 * @ClassName: WelcomeActivity
 * @Description:欢迎页面 引导界面
 */
public class WelcomeActivity extends BaseActivity {
	protected static final String TAG = WelcomeActivity.class.getSimpleName();
	// private final String firstStart = "firstStart" +
	// AppConfig.APP_VERSION_CODE;
	private Map<String, String> map;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		init();
		startActivity();
	}

	private void startActivity() {

		PreferencesUtils.clearUserData(getApplicationContext());// 每次进入都清除数据
		KeyUtils.cleardata(this);// 清除缓存的key数据
		map = KeyUtils.initKey(this);// 创建公私钥
		// System.out.println("公钥==" + map.get(KeyUtils.PUBLIC_KEY) + "-----");
		// System.out.println("私钥=="+map.get(KeyUtils.PRIVATE_KEY)+"----");
		String publickey = map.get(KeyUtils.PUBLIC_KEY);
		String privatekey = map.get(KeyUtils.PRIVATE_KEY);
		keyUtils.RequestKey(publickey, privatekey, "", this, loadingDialog);// 请求aes密钥

		// KeyUtils.getyzm(this, privatekey);

		// boolean isFristStart = PreferencesUtils.getBoolean(
		// getApplicationContext(), firstStart, true);
		// if (isFristStart) {
		// PreferencesUtils.putBoolean(getApplicationContext(), firstStart,
		// false);
		// Intent mainIntent = new Intent(WelcomeActivity.this,
		// GuideStartActivity.class);
		// WelcomeActivity.this.startActivity(mainIntent);
		// WelcomeActivity.this.finish();
		// } else {

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
				startActivity(intent);
				WelcomeActivity.this.finish();
			}

		}, 2000);

		// }
	}

	@Override
	protected void findViewById() {

	}

	@Override
	protected void setListener() {

	}

	@Override
	protected void init() {
	}

}
