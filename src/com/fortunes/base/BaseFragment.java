package com.fortunes.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.EYgo.R;
import com.fortunes.key.KeyUtils;
import com.fortunes.view.LoadingDialog;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

/**
 * @ClassName: BaseFragment
 * @Description: Fragment基类
 * @author liuyongzheng
 * @date 2014-3-18 下午2:38:21
 */
public abstract class BaseFragment extends Fragment implements OnTouchListener {
	protected static final String TAG = BaseFragment.class.getSimpleName();
	protected LoadingDialog loadingDialog = null;
	private int index;
	protected Gson gson;
	protected KeyUtils keyUtils;
	protected int count = 0;// 重复请求密钥次数

	/**
	 * 解决事件穿透问题
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		view.setOnTouchListener(this);
		super.onViewCreated(view, savedInstanceState);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gson = new Gson();
		keyUtils = new KeyUtils();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart(TAG);
	}

	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd(TAG);
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/** 绑定界面UI **/
	protected abstract void findViewById();

	/** 界面UI事件监听 **/
	protected abstract void setListener();

	/** 界面数据初始化 **/
	protected abstract void init();

	/**
	 * 显示下载提示框
	 */
	public void initProgressDialog() {
		if (loadingDialog == null) {
			loadingDialog = new LoadingDialog(getActivity(), R.style.loading_dialog);
			loadingDialog.setText("加载中...");
		}
		if (!getActivity().isFinishing() && !loadingDialog.isShowing()) {
			loadingDialog = new LoadingDialog(getActivity(), R.style.loading_dialog);
			loadingDialog.setText("加载中...");
			loadingDialog.show();
		}
		loadingDialog.setCanceledOnTouchOutside(true);
	}

	/**
	 * 关闭下载提示框
	 */
	public void dismissProgressDialog() {
		if (loadingDialog != null && loadingDialog.isShowing()) {
			loadingDialog.dismiss();
		}
	}

	/**
	 * 解决fragment事件穿透问题
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}
}
