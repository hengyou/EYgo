package com.fortunes.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.EYgo.R;
import com.fortunes.http.https;
import com.fortunes.key.KeyUtils;
import com.fortunes.view.LoadingDialog;
import com.google.gson.Gson;

/**
 * @ClassName: BaseFragmentActivity
 * @Description:FragmentActivity基类
 * @author: JimmyWang
 * @date 2015年11月5日下午3:13:40
 */
public abstract class BaseFragmentActivity extends FragmentActivity {
	@SuppressWarnings("unused")
	private static final String TAG = BaseFragmentActivity.class.getSimpleName();
	/**
	 * 屏幕的宽度和高度
	 */
	protected int mScreenWidth;
	protected int mScreenHeight;
	protected LoadingDialog loadingDialog = null;
	protected KeyUtils keyUtils;
	protected Gson gson;
	protected int count = 0;// 重复请求密钥次数

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppManager.getAppManager().addActivity(this);
		/**
		 * 获取屏幕宽度和高度
		 */
		DisplayMetrics metric = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metric);
		mScreenWidth = metric.widthPixels;
		mScreenHeight = metric.heightPixels;
		gson = new Gson();
		keyUtils = new KeyUtils();
		// LogUtils.i(TAG, "mScreenWidth=" + mScreenWidth + "--="
		// + getResources().getDimension(R.dimen.forty_five_dp));
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@SuppressWarnings("static-access")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// // 结束Activity&从堆栈中移除
		// System.out.println("清除请求");
		https.cancelRequests(this, true);
		// BaseApplication.getInstance().cancelPendingRequests(this);
		AppManager.getAppManager().finishActivity(this);
	}

	/** 绑定界面UI **/
	protected abstract void findViewById();

	/** 界面UI事件监听 **/
	protected abstract void setListener();

	/** 界面数据初始化 **/
	protected abstract void init();

	/** 短暂显示Toast提示(来自res) **/
	protected void showShortToast(int resId) {
		Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
	}

	/** 短暂显示Toast提示(来自String) **/
	protected void showShortToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	/** 长时间显示Toast提示(来自res) **/
	protected void showLongToast(int resId) {
		Toast.makeText(this, getString(resId), Toast.LENGTH_LONG).show();
	}

	/** 长时间显示Toast提示(来自String) **/
	protected void showLongToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}

	/** 含有标题和内容的对话框 **/
	protected AlertDialog showAlertDialog(String title, String message) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).show();
		return alertDialog;
	}

	/** 含有标题、内容、两个按钮的对话框 **/
	protected AlertDialog showAlertDialog(String title, String message, String positiveText,
			DialogInterface.OnClickListener onPositiveClickListener, String negativeText,
			DialogInterface.OnClickListener onNegativeClickListener) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message)
				.setPositiveButton(positiveText, onPositiveClickListener)
				.setNegativeButton(negativeText, onNegativeClickListener).show();
		return alertDialog;
	}

	/** 含有标题、内容、图标、两个按钮的对话框 **/
	protected AlertDialog showAlertDialog(String title, String message, int icon, String positiveText,
			DialogInterface.OnClickListener onPositiveClickListener, String negativeText,
			DialogInterface.OnClickListener onNegativeClickListener) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setIcon(icon)
				.setPositiveButton(positiveText, onPositiveClickListener)
				.setNegativeButton(negativeText, onNegativeClickListener).show();
		return alertDialog;
	}

	/**
	 * 显示下载提示框
	 */
	public void initProgressDialog() {
		if (loadingDialog == null) {
			loadingDialog = new LoadingDialog(this, R.style.loading_dialog);
			loadingDialog.setText("加载中...");
		}
		if (!this.isFinishing() && !loadingDialog.isShowing()) {
			loadingDialog = new LoadingDialog(this, R.style.loading_dialog);
			loadingDialog.setText("加载中...");
			loadingDialog.show();
		}
		loadingDialog.setCanceledOnTouchOutside(true);
	}

	/**
	 * 关闭下载提示框
	 */
	public void dismissProgressDialog() {
		if (loadingDialog != null && loadingDialog.isShowing()) {
			loadingDialog.dismiss();
		}
	}

}
