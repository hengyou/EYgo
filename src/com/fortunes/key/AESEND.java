package com.fortunes.key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

/**
 * @ClassName: AESEND
 * @Description:AES加解密工具类
 */
public class AESEND {

	public static String iv = "1234567890123456";

	/**
	 * @Description:AES 加密方法
	 * @param content
	 *            (需要加密的内容)
	 * @param password
	 *            (加密密码)
	 * @return (对加密的结果进行base64编码)
	 * 
	 */
	public static String encrypt(String content, String password) throws Exception {

		byte[] enCodeFormat = new BASE64Decoder().decodeBuffer(password);// 对秘钥进行解码
		SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] byteContent = content.getBytes("utf-8");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
		byte[] result = cipher.doFinal(byteContent);
		return new BASE64Encoder().encode(result); // 对加密的结果进行base64编码

	}

	/**
	 * @Description:注意：解密的时候要传入byte数组 解密
	 * @param contentStr
	 *            (待解密内容)
	 * @param password
	 *            (解密密钥)
	 * @return 解密后格式为utf-8的String
	 */
	public static String decrypt(String contentStr, String password) throws Exception {

		byte[] content = new BASE64Decoder().decodeBuffer(contentStr);// 对密文先解码
		byte[] enCodeFormat = new BASE64Decoder().decodeBuffer(password);// 对秘钥进行解码
		SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes()));
		byte[] result = cipher.doFinal(content);
		return new String(result, "utf-8"); // 解密
	}

	/**
	 * 将16进制转换为二进制
	 * 
	 * @param hexStr
	 * @return
	 */
	public static byte[] parseHexStr2Byte(String hexStr) {
		if (hexStr.length() < 1)
			return null;
		byte[] result = new byte[hexStr.length() / 2];
		for (int i = 0; i < hexStr.length() / 2; i++) {
			int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
			result[i] = (byte) (high * 16 + low);
		}
		return result;
	}
}