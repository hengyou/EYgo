package com.fortunes.key;

import java.util.Map;

import org.apache.http.Header;

import android.content.Context;

import com.EYgo.R;
import com.fortunes.config.Constant;
import com.fortunes.http.Urls;
import com.fortunes.http.https;
import com.fortunes.util.LogUtils;
import com.fortunes.util.NetUtils;
import com.fortunes.util.PreferencesUtils;
import com.fortunes.util.StringUtils;
import com.fortunes.util.ToastUtils;
import com.fortunes.view.LoadingDialog;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * @ClassName: KeyUtils
 * @Description: 公钥、密钥 工具类
 */
public class KeyUtils {
	/**
	 * 公钥的key
	 */
	public final static String PUBLIC_KEY = "RSAPublicKey";
	/**
	 * 私钥的key
	 */
	public final static String PRIVATE_KEY = "RSAPrivateKey";
	/**
	 * 服务器AESkey
	 */
	public final static String SERVER_AES_KEY = "ServerAESKey";

	/**
	 * 服务器AESkey
	 */
	// public final static String SERVER_PUBLIC_KEY = "ServerPublicKey";

	/**
	 * 生成app公钥和密钥
	 * 
	 * @param context
	 * @return
	 */
	public static Map<String, String> initKey(Context context) {// 产生公钥和密钥
		try {
			Map<String, String> map = new RSAUtils().genKeyPair();
			PreferencesUtils.putString(context, PUBLIC_KEY, map.get(PUBLIC_KEY));
			PreferencesUtils.putString(context, PRIVATE_KEY, map.get(PRIVATE_KEY));
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 密钥交互请求
	 * 
	 * @param publicKey
	 *            公钥
	 * @param privateKey
	 *            私钥
	 * @param name
	 *            发起重新请求秘钥的请求方法名字，没有的时候为"go"
	 * @param context
	 * @param loadingDialog
	 */
	public void RequestKey(String publicKey, final String privateKey, final String name, final Context context,
			final LoadingDialog loadingDialog) {// 密钥交互请求
		if (NetUtils.isNetConnected(context)) {
			RequestParams rp = new RequestParams();
			rp.put("appRsaPubKey", publicKey);
			https.get(context, Urls.KEY_URL, rp, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(String response) {
					if (loadingDialog != null && loadingDialog.isShowing()) {
						loadingDialog.dismiss();
					}
					if (!StringUtils.isEmpty(response)) {
						LogUtils.i(":response=", response);
						Gson gson = new Gson();
						KeyBean b = gson.fromJson(response, KeyBean.class);
						if (b.getState() == null || !b.getState().equals(Constant.HTTP_SUCCESS)) {
							return;
						}
						String JMserverAESkey = b.getSzsbAesKey();// 服务器加密的asekey
						// String serverPublickey =
						// b.getPosRsaPubKey();// 服务器公钥
						// PreferencesUtils.putString(context,
						// SERVER_PUBLIC_KEY, serverPublickey);
						String serverAESkey = JmAESkey(context, JMserverAESkey, privateKey);
						// System.out.println("解密成功");
						if (rKeyListener != null) {
							if (StringUtils.isEmpty(name)) {
								rKeyListener.callBack("go");
							} else {
								rKeyListener.callBack(name);
							}
						}
						// getyzm(context, privateKey);
					}
				}

				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					LogUtils.i(":response=", "请求失败");
					ToastUtils.showShortToast(context, "网络请求错误");
					if (loadingDialog != null && loadingDialog.isShowing()) {
						loadingDialog.dismiss();
					}
				}
			});
		} else {
			if (loadingDialog != null && loadingDialog.isShowing()) {
				loadingDialog.dismiss();
			}
			ToastUtils.showShortToast(context, context.getString(R.string.net_not_connected));
		}
	}

	private RequestKeyListener rKeyListener = null;

	public void setOnRequestKeyListener(RequestKeyListener KeyListener) {
		this.rKeyListener = KeyListener;
	};

	public interface RequestKeyListener {
		void callBack(String go);
	};

	// public static void getyzm(final Context context, final String privatekey)
	// {
	// // AsyncHttpClient client = new AsyncHttpClient();
	// RequestParams rp = new RequestParams();
	// String sd = AESEncrypt("1014407",
	// PreferencesUtils.getString(context, SERVER_AES_KEY));
	// String cd = AESEncrypt("123456",
	// PreferencesUtils.getString(context, SERVER_AES_KEY));
	// rp.put("id", sd);
	// rp.put("code", cd);
	// https.get(context, Urls.BIND_FAMILY_INFO_URL, rp,
	// new AsyncHttpResponseHandler() {
	// @Override
	// public void onSuccess(String response) {
	// if (!StringUtils.isEmpty(response)) {
	// LogUtils.i(":response=", response);
	// // 解析 判断是不是数组是不是为空,空就显示添加界面
	// Gson gson = new Gson();
	// EncryptDataBean bean = gson.fromJson(response,
	// EncryptDataBean.class);
	// // if (bean.getcode()) {//判断密钥是不是过期
	// //
	// // }
	// // String aeskey = JmAESkey(context, bean.getKey(),
	// // privatekey);
	// // System.out.println("aes=="+aeskey);
	// String aeskey = PreferencesUtils.getString(context,
	// SERVER_AES_KEY);
	// if (!StringUtils.isEmpty(aeskey)) {
	// String datajson = AESdecrypt(bean.getData(),
	// aeskey);
	// LogUtils.i(":response=", datajson);
	// FamliyBean beanx = gson.fromJson(datajson,
	// FamliyBean.class);
	//
	// } else {
	//
	// }
	// }
	// }
	//
	// @Override
	// public void onFailure(int arg0, Header[] arg1, byte[] arg2,
	// Throwable arg3) {
	// LogUtils.i(":response=", "请求失败");
	// ToastUtils.showShortToast(context, "网络请求错误");
	// }
	// });
	// }

	/**
	 * 清除所有密钥相关数据
	 * 
	 * @param context
	 */
	public static void cleardata(Context context) {// 清除密钥相关的数据
		PreferencesUtils.putString(context, PUBLIC_KEY, "");
		PreferencesUtils.putString(context, PRIVATE_KEY, "");
		PreferencesUtils.putString(context, SERVER_AES_KEY, "");
		// PreferencesUtils.putString(context, SERVER_PUBLIC_KEY, "");
	}

	/**
	 * 清除AES密钥相关数据
	 * 
	 * @param context
	 */
	public static void cleardata2(Context context) {// 清除AES密钥相关数据
		PreferencesUtils.putString(context, SERVER_AES_KEY, "");
	}

	/**
	 * 判断有没服务器AES密钥
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isKeyExchange(Context context) {
		if (!StringUtils.isEmpty(PreferencesUtils.getString(context, SERVER_AES_KEY))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 解密 AES密文方法
	 * 
	 * @param context
	 * @param AESkey
	 * @param privatekey
	 * @return
	 */
	public static String JmAESkey(Context context, String AESkey, String privatekey) {// 解密aes密文
		try {
			Base64Utils base64Utils = new Base64Utils();
			byte[] s = base64Utils.decode(AESkey);
			byte[] aes = new RSAUtils().decryptByPrivateKey(s, privatekey);
			String serverAESkey = new String(aes);
			LogUtils.i("aes密钥=", serverAESkey);
			PreferencesUtils.putString(context, SERVER_AES_KEY, serverAESkey);
			return serverAESkey;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogUtils.i("aes密钥=", "解密aes失败");
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * AES加密方法
	 * 
	 * @param string
	 * @param AESkey
	 * @return
	 */
	public static String AESEncrypt(String string, String AESkey) {
		try {
			String sd = AESEND.encrypt(string, AESkey); // 数据aes加密
			return sd;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * AES解密方法
	 * 
	 * @param string
	 * @param AESkey
	 * @return
	 */
	public static String AESdecrypt(String string, String AESkey) {
		try {
			String sd = AESEND.decrypt(string, AESkey); // 数据aes解密
			return sd;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("解密失败");
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 重新请求密钥交互
	 * 
	 * @param context
	 *            上下文
	 * @param loadingDialog
	 *            加载提示框
	 * @param count
	 *            记录的重复请求的次数
	 * @param toast
	 *            取消这次请求的提示语句，不写默认为“返回数据”；
	 * @return 已经请求的次数
	 */
	public int RerequestKey(Context context, LoadingDialog loadingDialog, int count, String toast) {
		if (count == 2) {// 设置多次重复请求3次就放弃这一次请求；
			KeyUtils.cleardata2(context);
			count = 0;
			if (StringUtils.isEmpty(toast)) {
				ToastUtils.showShortToast(context, "数据异常");
			} else {
				ToastUtils.showShortToast(context, toast);
			}
			return count;
		}
		count++;
		if (StringUtils.isEmpty(PreferencesUtils.getString(context, KeyUtils.PUBLIC_KEY))
				|| StringUtils.isEmpty(PreferencesUtils.getString(context, KeyUtils.PRIVATE_KEY))) {
			initKey(context);
		}
		String publickey = PreferencesUtils.getString(context, KeyUtils.PUBLIC_KEY);
		String privatekey = PreferencesUtils.getString(context, KeyUtils.PRIVATE_KEY);
		RequestKey(publickey, privatekey, "", context, loadingDialog);
		return count;
	}

	/**
	 * 重新请求密钥交互
	 * 
	 * @param context
	 *            上下文
	 * @param loadingDialog
	 *            加载提示框
	 * @param count
	 *            记录的重复请求的次数
	 * @param RerequestName
	 *            从哪个请求中重新去请求密钥
	 * @param toast
	 *            取消这次请求的提示语句，不写默认为“返回数据”；
	 * @return 已经请求的次数
	 */
	public int RerequestKey(Context context, LoadingDialog loadingDialog, int count, String RerequestName,
			String toast) {
		if (count == 2) {// 设置多次重复请求3次就放弃这一次请求；
			KeyUtils.cleardata2(context);
			count = 0;
			if (StringUtils.isEmpty(toast)) {
				ToastUtils.showShortToast(context, "数据异常");
			} else {
				ToastUtils.showShortToast(context, toast);
			}
			return count;
		}
		count++;
		if (StringUtils.isEmpty(PreferencesUtils.getString(context, KeyUtils.PUBLIC_KEY))
				|| StringUtils.isEmpty(PreferencesUtils.getString(context, KeyUtils.PRIVATE_KEY))) {
			initKey(context);
		}
		String publickey = PreferencesUtils.getString(context, KeyUtils.PUBLIC_KEY);
		String privatekey = PreferencesUtils.getString(context, KeyUtils.PRIVATE_KEY);
		RequestKey(publickey, privatekey, RerequestName, context, loadingDialog);
		return count;
	}
}
