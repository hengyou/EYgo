package com.fortunes.key;

/**
 * @ClassName: KeyBean
 * @Description:密钥交互数据类
 */
public class KeyBean {
	public String msg;
	public String state;
	public String szsbAesKey;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSzsbAesKey() {
		return szsbAesKey;
	}

	public void setSzsbAesKey(String szsbAesKey) {
		this.szsbAesKey = szsbAesKey;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
