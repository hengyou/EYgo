package com.fortunes.key;

/**
 * @ClassName: EncryptDataBean
 * @Description:用于保存加密数据
 */
public class EncryptDataBean {
	private String state;
	private String data;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
