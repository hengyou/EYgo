package com.fortunes.http;

/**
 * @ClassName: Urls
 * @Description: 请求数据的URL
 * @author 
 * @date 2014-3-19 下午2:12:45
 */
public class Urls {
	public static final String BASE_URL = "http://183.60.136.208:8081/szsb/";//主服务器地址
//	public static final String BASE_URL = "http://10.9.4.57:8080/szsb/";//测试服务器地址1
//	public static final String BASE_URL = "http://192.168.8.5:9001/szsb/";//测试服务器地址2
	
	

	public static final String KEY_URL = BASE_URL+"usersAppI/keyChange";// 密钥交互
//	public static final String BD_URL = "http://10.9.4.58:8080/szsb/usersAppI/bindFamilyInfo";// 
	
	public static final String DOWN_URL = BASE_URL+ "usersAppI/downLoadAPK";// 更新apk下载地址
	public static final String REGISTRATION_URL = BASE_URL+ "usersAppI/registration";// 注册
	
	public static final String LOGIN_URL = BASE_URL+"usersAppI/login";// 登录
	
	public static final String LOGOUT_URL = BASE_URL+ "usersAppI/loginOut";// 注销
	public static final String VERIFYPHONE_URL = BASE_URL+ "usersAppI/verifyPhone";// 验证手机号码
	public static final String VERIFYCODE_URL = BASE_URL+ "usersAppI/verifyEmail";// 获取验证码
	public static final String NEWVERSION_URL = BASE_URL+ "usersAppI/newVersion";// 版本更新
	
	//信息界面
	public static final String GET_PROF_SCORES_URL = BASE_URL+ "usersAppI/getProfScores";// 职业成绩
	public static final String GET_PROF_EXAM_URL = BASE_URL+ "usersAppI/getProfExam";// 职业考场
	public static final String GET_PROF_CERT_URL = BASE_URL+ "usersAppI/getProfCert";// 职业证书
	public static final String GET_LOW_WAGE = BASE_URL+"infomationAppI/getMinWage";//我市最低工资
	public static final String GET_TRAIN_MECH = BASE_URL+"infomationAppI/getProfTrainOrg";//职业培训机构名单
	public static final String GET_SKILL_APPRAISAL= BASE_URL+"infomationAppI/getProfAuthOrg";//职业技能鉴定机构名单
	public static final String GET_SCHOOL_LIST = BASE_URL + "infomationAppI/getSchInfo";// 技工学校名单
	//业务办理界面
	public static final String BIND_FAMILY_INFO_URL = BASE_URL+ "usersAppI/bindFamilyInfo";// 个人家庭绑定情况
	public static final String BIND_FAMILY_URL = BASE_URL+ "usersAppI/bindFamily";// 添加家庭绑定
	public static final String GET_SKORG_URL = BASE_URL+ "usersAppI/getSKOrg";// 获取社康机构列表
	public static final String CHANGE_SKORG_URL = BASE_URL+ "usersAppI/changeSKOrg";// 变更社康绑定
	//社保转移界面
	public static final String INNER_BYPROVINCE_URL = BASE_URL+ "socialsAppI/innerByProvince";// 省内养老
	public static final String OUTER_BYPROVINCE_URL = BASE_URL+ "socialsAppI/outerByProvince";// 省外养老
	public static final String INTO_HEALINSUR_URL = BASE_URL+ "socialsAppI/intoHealInsur";// 医疗保险
	
	// 参保查询
	public static final String GET_PEN_PAY_AFTER = BASE_URL + "socialsAppI/laterByAugust";// 92年8月以后养老保险缴交明细
	public static final String GET_PEN_PAY_BEFORE = BASE_URL + "socialsAppI/beforeByAugust";// 92年8月以前养老保险缴交明细
	public static final String GET_PEN_PAY_YEAR = BASE_URL + "socialsAppI/getPastInsurance";// 近半年养老保险发放及补发明细
	public static final String GET_MED_PAY = BASE_URL + "socialsAppI/getHealInsurFee";// 医疗保险缴交明细
	public static final String GET_MED_DETAIL = BASE_URL + "socialsAppI/getHealInsurCsm";// 医疗保险消费明细
	public static final String GET_JOB_INJURY = BASE_URL + "socialsAppI/getIndustryInsurFee";// 工伤保险缴交明细
	public static final String GET_INJURY_ENJOY = BASE_URL + "socialsAppI/getIndustryInsurCsm";// 工伤保险待遇享受
	public static final String GET_LOST_JOB = BASE_URL + "socialsAppI/getUnempInsurFee";// 失业保险缴交明细
	public static final String GET_LOST_JOB_KIM = BASE_URL + "socialsAppI/getUnempInsurCsm";// 失业金领取信息

	// 个人参保信息
	/** 个人参保信息 **/
	public static final String GET_PERSON_INSURED_INFO = BASE_URL + "usersAppI/getPerInfo";// 个人参保信息
	public static final String GET_PERSON_BALANCE = BASE_URL + "usersAppI/getPerBalance";// 个人账户余额

	// 权益单
	/** url-->个人权益记录 **/
	public static final String GET_PERSON_EQUITY_RECORD = BASE_URL + "usersAppI/getRightsRecord";// 个人权益记录
	/** url-->个人参保凭证 **/
	public static final String GET_PERSON_INSURED_ENVI = BASE_URL + "usersAppI/getRightsEnvi";// 个人参保凭证
		
	/** url-->缴纳社保*/
	public static final String GET_PAYINSURED_TN=BASE_URL+"usersAppI/payment";
	public static final String GET_AGAIN_TN=BASE_URL+"usersAppI/getTN";
	

	
	/** url-->查询订单 */
	public static final String GET_ORDER_INFO = BASE_URL+"usersAppI/queryOrders";
	public static final String GET_CANCEL_ORDER = BASE_URL+"usersAppI/cancelOrders";//取消订单
	public static final String GET_DEL_ORDER = BASE_URL+"usersAppI/delOrders";//删除订单
	public static final String GET_UPDATA_ORDER = BASE_URL+"usersAppI/updateOrders";//更新订单状态
	public static final String GET_TN_AGAIN = BASE_URL+"usersAppI/getTN";

}
