package com.fortunes.http;

import java.io.UnsupportedEncodingException;

import android.content.Context;

import com.EYgo.R;
import com.fortunes.util.LogUtils;
import com.fortunes.util.NetUtils;
import com.fortunes.util.ToastUtils;
import com.fortunes.view.LoadingDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * @ClassName: https
 * @Description:网络请求工具类
 */
public class https {
	private static AsyncHttpClient client = new AsyncHttpClient(); // 实例话对象

	static {
		client.setTimeout(10000); // 设置链接超时，如果不设置，默认为10s
	}

	/**
	 * 用一个完整url获取一个string对象
	 * 
	 * @param context
	 * @param urlString
	 * @param res
	 */
	public static void get(Context context, String urlString, AsyncHttpResponseHandler res) {
		LogUtils.i("Http", "URL=" + urlString);
		client.get(context, urlString, res);
	}

	/**
	 * 带检查网络的功能的请求
	 * @param loadingDialog
	 * @param urlString
	 * @param params
	 * @param res
	 */
	public static void get(Context context, LoadingDialog loadingDialog, String urlString, RequestParams params,
			AsyncHttpResponseHandler res) {
		try {
			LogUtils.i("Http", "URL="
					+ java.net.URLDecoder.decode(client.getUrlWithQueryString(true, urlString, params), "UTF-8"));
			// System.out.println("URL="
			// + java.net.URLDecoder.decode(client.getUrlWithQueryString(
			// true, urlString, params), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (NetUtils.isNetConnected(context)) {
			client.get(context, urlString, params, res);
		} else {
			if (loadingDialog != null && loadingDialog.isShowing()) {
				loadingDialog.dismiss();
			}
			ToastUtils.showShortToast(context, context.getString(R.string.net_not_connected));
		}

	}

	/**
	 * 没有检查网络的功能的请求
	 * @param loadingDialog
	 * @param urlString
	 * @param params
	 * @param res
	 */
	public static void get(Context context, String urlString, RequestParams params, AsyncHttpResponseHandler res) {
		try {
			LogUtils.i("Http", "URL="
					+ java.net.URLDecoder.decode(client.getUrlWithQueryString(true, urlString, params), "UTF-8"));
			// System.out.println("URL="
			// + java.net.URLDecoder.decode(client.getUrlWithQueryString(
			// true, urlString, params), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		client.get(context, urlString, params, res);

	}

	/**
	 * 不带参数，获取json对象或者数组
	 * 
	 * @param urlString
	 * @param res
	 */
	public static void get(String urlString, JsonHttpResponseHandler res) {
		client.get(urlString, res);

	}

	/**
	 * 带参数，获取json对象或者数组
	 * 
	 * @param urlString
	 * @param params
	 * @param res
	 */
	public static void get(String urlString, RequestParams params, JsonHttpResponseHandler res) {

		client.get(urlString, params, res);

	}

	/**
	 * 下载数据使用，会返回byte数据
	 * 
	 * @param uString
	 * @param bHandler
	 */
	public static void get(String uString, BinaryHttpResponseHandler bHandler) {
		LogUtils.i("Http", "URL=" + uString);
		client.get(uString, bHandler);
	}

	public static AsyncHttpClient getClient() {
		return client;
	}

	/** * 取消网络请求。 * @param context * @param mayInterruptIfRunning */
	public static void cancelRequests(Context context, boolean mayInterruptIfRunning) {
		if (client != null) {
			client.cancelRequests(context, mayInterruptIfRunning);
		}
	}

}
